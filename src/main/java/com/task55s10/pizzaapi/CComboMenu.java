package com.task55s10.pizzaapi;

public class CComboMenu {
    private String name = "";
    private int duongkinh = 0;
    private int suonnuong = 0;
    private int salad = 0;
    private int nuocngot = 0;
    private int thanhtien = 0;
    
    public CComboMenu(String name, int duongkinh, int suonnuong, int salad, int nuocngot, int thanhtien) {
        this.name = name;
        this.duongkinh = duongkinh;
        this.suonnuong = suonnuong;
        this.salad = salad;
        this.nuocngot = nuocngot;
        this.thanhtien = thanhtien;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuongkinh() {
        return duongkinh;
    }

    public void setDuongkinh(int duongkinh) {
        this.duongkinh = duongkinh;
    }

    public int getSuonnuong() {
        return suonnuong;
    }

    public void setSuonnuong(int suonnuong) {
        this.suonnuong = suonnuong;
    }

    public int getSalad() {
        return salad;
    }

    public void setSalad(int salad) {
        this.salad = salad;
    }

    public int getNuocngot() {
        return nuocngot;
    }

    public void setNuocngot(int nuocngot) {
        this.nuocngot = nuocngot;
    }

    public int getThanhtien() {
        return thanhtien;
    }

    public void setThanhtien(int thanhtien) {
        this.thanhtien = thanhtien;
    }

    @Override
    public String toString() {
        return "CComboMenu [duongkinh=" + duongkinh + ", name=" + name + ", nuocngot=" + nuocngot + ", salad=" + salad
                + ", suonnuong=" + suonnuong + ", thanhtien=" + thanhtien + "]";
    }

    
}
